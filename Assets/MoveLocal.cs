﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLocal : MonoBehaviour
{
    public Transform goal;
    [SerializeField] float speed = 0.5f;
    [SerializeField] float accuracy = 1.0f;
    [SerializeField] float rotationSpeed = 0.5f;

    Vector3 lookAtGoal;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
        Vector3 direction = lookAtGoal - transform.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);

        if (Vector3.Distance(transform.position, lookAtGoal) > accuracy)
        {
            transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, lookAtGoal);
        Gizmos.DrawWireSphere(lookAtGoal, accuracy);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(goal.position, lookAtGoal);
        Gizmos.DrawSphere(lookAtGoal, 0.15f);
    }
}
